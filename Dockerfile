ARG BASE_IMAGE

# Build final image.
FROM ${BASE_IMAGE}

# Adapt these variables to your needs.
# See https://hub.docker.com/_/mysql, Environment Variables section.
ENV MYSQL_ROOT_HOST="%"
ENV MYSQL_ROOT_PASSWORD="supersecretpassword"
ENV MYSQL_DATABASE="magento"
ENV MYSQL_USER="magento"
ENV MYSQL_PASSWORD="magento"
ENV MYSQL_ALLOW_EMPTY_PASSWORD=""
ENV MYSQL_RANDOM_ROOT_PASSWORD=""
ENV MYSQL_ONETIME_PASSWORD=""
ENV MYSQL_INITDB_SKIP_TZINFO=""

# Add custom config files.
COPY context/my.cnf /etc/mysql/my.cnf
COPY context/conf.d /etc/mysql/conf.d

# Fix custom config files permissions.
RUN chmod -R 644 /etc/mysql/

HEALTHCHECK --interval=30s --timeout=10s --start-period=30s --retries=3 \
  CMD mysqladmin ping -hlocalhost -u${MYSQL_USER} -p${MYSQL_PASSWORD}
